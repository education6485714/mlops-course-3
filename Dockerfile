FROM python:3.11-slim

ARG pdm_group

WORKDIR /app

# install PDM
RUN pip install -U pdm

# disable update check
ENV PDM_CHECK_UPDATE=false
# copy files
COPY pyproject.toml pdm.lock ./
RUN pdm install $pdm_group
ENV PATH="/app/.venv/bin:$PATH"

COPY src/ ./src
WORKDIR /app/src/mlops_course_3
ENTRYPOINT [ "python", "test.py" ]
