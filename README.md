# mlops course

This is a repo for mlops student project.
The link to course [here](https://ods.ai/tracks/mlops3-course-spring-2024)


Commands to build Docker images:
* for developer image 'docker build -t mlops-task3 .'
* fof prod image 'docker build -t mlops-task3 --build-arg pdm_group=--prod .'

Run Docker container very simple:
<br>
'docker run -it mlops-task3'
